﻿using Microsoft.EntityFrameworkCore;
using Shared.Models.Entities;

namespace Shared.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }
        public DbSet<User> Users { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Session> Sessions { get; set; }

        public DbSet<UserVerification> UserVerifications { get; set; }

        public DbSet<PasswordReset> PasswordResets { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            UserBuild(modelBuilder);

            RoleBuild(modelBuilder);

            modelBuilder.Entity<UserRole>()
                .HasKey(c => new { c.RoleId, c.UserId});

            SessionBuilder(modelBuilder);

            UserVerificationBuilder(modelBuilder);

            PasswordResetsBuilder(modelBuilder);

            RoleSeeder(modelBuilder);
        }

        private void UserBuild(ModelBuilder modelBuilder)
        {
            var userBuilder = modelBuilder.Entity<User>();
            userBuilder.HasIndex(u => u.Email).IsUnique();
            userBuilder.HasIndex(u => u.Name).IsUnique();
            userBuilder.Property(x => x.CreatedAt).HasDefaultValueSql("CURRENT_TIMESTAMP");
            userBuilder.Property(x => x.UpdatedAt).HasDefaultValueSql("CURRENT_TIMESTAMP");
        }

        private void RoleBuild(ModelBuilder modelBuilder)
        {
            var roleBuilder = modelBuilder.Entity<Role>();
            roleBuilder.HasIndex(r => r.SystemName).IsUnique();
            roleBuilder.Property(x => x.SystemName).IsFixedLength();
        }

        private void SessionBuilder(ModelBuilder modelBuilder)
        {
            var sessionBuilder = modelBuilder.Entity<Session>();
            sessionBuilder.Property(s => s.IsActive).HasDefaultValue(true);
            sessionBuilder.Property(x => x.CreatedAt).HasDefaultValueSql("CURRENT_TIMESTAMP");
            sessionBuilder.Property(x => x.UpdatedAt).HasDefaultValueSql("CURRENT_TIMESTAMP");
        }

        private void UserVerificationBuilder(ModelBuilder modelBuilder)
        {
            var verificationBuilder = modelBuilder.Entity<UserVerification>();

            verificationBuilder.HasIndex(uv => uv.Token);
            verificationBuilder.Property(uv => uv.Email).IsFixedLength();
            verificationBuilder.Property(uv => uv.Token).IsFixedLength();
            verificationBuilder.Property(uv => uv.CreatedAt).HasDefaultValueSql("CURRENT_TIMESTAMP");
        }

        private void PasswordResetsBuilder(ModelBuilder modelBuilder)
        {
            var passResetBuilder = modelBuilder.Entity<PasswordReset>();

            passResetBuilder.HasIndex(uv => uv.Token);
            passResetBuilder.Property(uv => uv.Email).IsFixedLength();
            passResetBuilder.Property(uv => uv.Token).IsFixedLength();
            passResetBuilder.Property(uv => uv.CreatedAt).HasDefaultValueSql("CURRENT_TIMESTAMP");
        }

        private void RoleSeeder(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>()
                .HasData(
                new Role { Id = 1, SystemName = "admin", Name = "Администратор" },
                new Role { Id = 2, SystemName = "user", Name = "Пользователь" },
                new Role { Id = 3, SystemName = "unverified", Name = "Аноним" });
        }
    }
}

