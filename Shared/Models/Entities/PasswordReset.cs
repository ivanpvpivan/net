﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Shared.Models.Entities
{
    public class PasswordReset
    {
        [Key]
        [Required, DataType(DataType.EmailAddress)]
        [StringLength(50)]
        public string Email { get; set; }

        [Required, StringLength(30)]
        public string Token { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime CreatedAt { get; set; }
    }
}
