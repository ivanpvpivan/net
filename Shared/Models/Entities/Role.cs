﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Shared.Models.Entities
{
    public class Role 
    {
        public Role()
        {
            Users = new HashSet<UserRole>();
        }
        [Key]
        public int Id { get; set; }

        [Required, MaxLength(25)]
        public string SystemName { get; set; }

        [Required, StringLength(50)]
        public string Name { get; set; }

        public virtual ICollection<UserRole> Users { get; set; }
    }
}
