﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Net;

namespace Shared.Models.Entities
{
    public class Session 
    {
        public Session() { }
        public Session(Session session)
        {
            Id = session.Id;
            User = session.User;
            IPAddressBytes = session.IPAddressBytes;
            UserAgent = session.UserAgent;
            IsActive = session.IsActive;
            RefreshToken = session.RefreshToken;
            ExpiredAfter = session.ExpiredAfter;
            CreatedAt = session.CreatedAt;
            UpdatedAt = session.UpdatedAt;
        }

        [Key]
        public Guid Id { get; set; }

        [Required]
        public virtual User User { get; set; }

        [Required, MinLength(4), MaxLength(16)]
        public byte[] IPAddressBytes { get; set; }

        [StringLength(255)]
        public string UserAgent { get; set; }
        public bool? IsActive { get; set; }
        
        [Required, StringLength(20)]
        public string RefreshToken { get; set; }

        [Required, DataType(DataType.DateTime)]
        public DateTime ExpiredAfter { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime CreatedAt { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime UpdatedAt { get; set; }

        [NotMapped]
        public IPAddress IPAddress
        {
            get { return new IPAddress(IPAddressBytes); }
            set { IPAddressBytes = value.GetAddressBytes(); }
        }
    }
}
