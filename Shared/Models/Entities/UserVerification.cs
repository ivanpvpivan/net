﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Shared.Models.Entities
{
    public class UserVerification
    {
        [Key]
        [DataType(DataType.EmailAddress), StringLength(50)]
        public string Email { get; set; }
        [StringLength(20)]
        public string Token { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime CreatedAt { get; set; }
    }
}
