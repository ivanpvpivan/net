﻿
using System.ComponentModel.DataAnnotations.Schema;

namespace Shared.Models.Entities
{
    [Table("UserRoles")]
    public class UserRole
    {
        public long UserId { get; set; }
        public virtual User User { get; set; }
        public int RoleId { get; set; }
        public virtual Role Role { get; set; }
    }
}
