﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Shared.Models.Entities
{
    public class User 
    {
        public User()
        {
            Roles = new HashSet<UserRole>();
            Sessions = new HashSet<Session>();

        }
        [Key]
        public long Id { get; set; }

        [Required, StringLength(50)]
        public string Email { get; set; }

        [Required, StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        [Required, StringLength(60)]
        public string Password { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? VerifiedAt { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime CreatedAt { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime UpdatedAt { get; set; }

        public virtual ICollection<UserRole> Roles { get; set; }

        public virtual ICollection<Session> Sessions { get; set; }
    }
}
