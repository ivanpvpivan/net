﻿using System;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore.Design;
using System.IO;

namespace Shared.Models
{
    class AppDbContextFactory : IDesignTimeDbContextFactory<AppDbContext>
    {
        public AppDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
            .SetBasePath(AppContext.BaseDirectory)
            .AddJsonFile(Directory.GetCurrentDirectory() + "/configs/db_config.json")
            .Build();

            var builder = new DbContextOptionsBuilder<AppDbContext>();
            
            builder.UseNpgsql(configuration["PostgreSQL:ConnectionString"]);

            return new AppDbContext(builder.Options);
        }
    }
}
