﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Shared.Migrations
{
    public partial class UserVerification : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserVerifications",
                columns: table => new
                {
                    Email = table.Column<string>(fixedLength: true, maxLength: 50, nullable: true),
                    Token = table.Column<string>(fixedLength: true, maxLength: 20, nullable: true),
                    CreatedAt = table.Column<DateTime>(nullable: false, defaultValueSql: "CURRENT_TIMESTAMP")
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserVerifications_Email",
                table: "UserVerifications",
                column: "Email",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_UserVerifications_Token",
                table: "UserVerifications",
                column: "Token");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserVerifications");
        }
    }
}
