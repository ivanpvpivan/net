﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Shared.Migrations
{
    public partial class SessionChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Initiator",
                table: "Sessions",
                newName: "UserAgent");

            migrationBuilder.AddColumn<DateTime>(
                name: "ExpiredAfter",
                table: "Sessions",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                table: "Sessions",
                nullable: true,
                defaultValue: true);

            migrationBuilder.AddColumn<string>(
                name: "RefreshToken",
                table: "Sessions",
                maxLength: 20,
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ExpiredAfter",
                table: "Sessions");

            migrationBuilder.DropColumn(
                name: "IsActive",
                table: "Sessions");

            migrationBuilder.DropColumn(
                name: "RefreshToken",
                table: "Sessions");

            migrationBuilder.RenameColumn(
                name: "UserAgent",
                table: "Sessions",
                newName: "Initiator");
        }
    }
}
