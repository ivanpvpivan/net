﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Shared.Migrations
{
    public partial class VerificationTokenKey : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_UserVerifications_Email",
                table: "UserVerifications");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "UserVerifications",
                fixedLength: true,
                maxLength: 50,
                nullable: false,
                oldClrType: typeof(string),
                oldType: "character(50)",
                oldFixedLength: true,
                oldMaxLength: 50,
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserVerifications",
                table: "UserVerifications",
                column: "Email");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_UserVerifications",
                table: "UserVerifications");

            migrationBuilder.AlterColumn<string>(
                name: "Email",
                table: "UserVerifications",
                type: "character(50)",
                fixedLength: true,
                maxLength: 50,
                nullable: true,
                oldClrType: typeof(string),
                oldFixedLength: true,
                oldMaxLength: 50);

            migrationBuilder.CreateIndex(
                name: "IX_UserVerifications_Email",
                table: "UserVerifications",
                column: "Email",
                unique: true);
        }
    }
}
