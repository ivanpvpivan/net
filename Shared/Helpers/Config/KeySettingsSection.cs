﻿using System.Collections.Generic;

namespace Shared.Helpers.Config
{
    public class KeySettingsSection
    {
        public string AppKey { get; set; }
        public uint Ttl { get; set; }
        public uint RefreshTtl { get; set; }
        public IEnumerable<string> Issuers { get; set; }
    }
}
