﻿using System;
using System.Security.Cryptography;

namespace Shared.Helpers
{
    public class Salt
    {
        public static string Create()
        {
            byte[] randomBytes = new byte[1024 / 8];
            using (var generator = RandomNumberGenerator.Create())
            {
                generator.GetBytes(randomBytes);
                return Convert.ToBase64String(randomBytes);
            }
        }
    }
}
