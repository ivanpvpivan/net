﻿using static BCrypt.Net.BCrypt;

namespace Shared.Helpers
{
    public static class PasswordHelper
    {
        public static bool Validate(string candidate, string hash) =>
            Verify(candidate, hash);
        public static string Make(string password) => 
            HashPassword(password);
   

    }
}
