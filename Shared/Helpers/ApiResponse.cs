﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Shared.Helpers.Response
{
    public class ApiResponse : IActionResult
    {
        public ApiResponse(object content = null, object props = null, string msg = "success", ushort code = 200)
        {
            Code = code;
            Data = new ResponseStruct {
                Content = content == null ? new object[0] : content,
                Msg = msg,
                Props = props == null ? new { } : props
            };
        }
        public async Task ExecuteResultAsync(ActionContext context)
        {
            context.HttpContext.Response.StatusCode = Code;
            context.HttpContext.Response.ContentType = "application/json";
            if (Code == 204)
                await context.HttpContext.Response.WriteAsync(null);
            else
            {
                string resp = JsonConvert.SerializeObject(Data);
                await context.HttpContext.Response.WriteAsync(resp);
            }
        }
        private ushort Code { get; set; }
        private ResponseStruct Data { get; set; }
        private struct ResponseStruct
        {
            public object Content { get; set; }
            public string Msg { get; set; }
            public object Props { get; set; }
        }
    }
}
