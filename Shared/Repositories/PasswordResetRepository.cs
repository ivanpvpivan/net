﻿using System.Threading.Tasks;
using System.Text.RegularExpressions;

using Microsoft.EntityFrameworkCore;

using Shared.Models;
using Shared.Helpers;
using Shared.Models.Entities;
using Shared.Repositories.Contracts;

namespace Shared.Repositories
{
    public class PasswordResetRepository : BaseRepository<PasswordReset>, IPasswordResetRepository
    {
        public PasswordResetRepository(AppDbContext context):base(context)
        {

        }
        public PasswordReset CreateToken(string email)
        {
            PasswordReset token = new PasswordReset
            {
                Email = email,
                Token = Regex.Replace(Salt.Create().Substring(0, 30), @"[\\/\+]", "a")
            };

            Insert(token);
            return token;
        }

        public Task<PasswordReset> FindByTokenAsync(string token) =>
            GetByCondition(uv => uv.Token == token).FirstOrDefaultAsync();
    }
}
