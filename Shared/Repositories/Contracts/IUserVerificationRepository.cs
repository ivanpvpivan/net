﻿using System.Threading.Tasks;

using Shared.Models.Entities;

namespace Shared.Repositories.Contracts
{
    public interface IUserVerificationRepository : IRepository<UserVerification>
    {
        public UserVerification CreateToken(string email);

        public Task<UserVerification> FindByTokenAsync(string token);
    }
}
