﻿using System.Net;
using Shared.Models.Entities;

namespace Shared.Repositories.Contracts
{
    public interface ISessionRepository : IRepository<Session>
    {
        public Session GenerateSession(string userAgent, IPAddress ip, uint ttl);

        public Session RefreshSession(Session session, uint ttl);

        public void DisableSession(Session session);
    }
}
