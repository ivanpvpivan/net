﻿using System.Threading.Tasks;

using Shared.Models.Entities;

namespace Shared.Repositories.Contracts
{
    public interface IUserRepository : IRepository<User>
    {
        public Task<User> GetUserByIdAsync(long id);
        public Task<User> GetUserWithRolesByIdAsync(long id);
        public Task<User> GetUserByEmailAsync(string email);
        public Task<User> GetUserWithRolesByEmailAsync(string email);
        public Task<User> GetTrackedUserByEmailAsync(string email);
        public Task<User> GetTrackedUserWithRolesByEmailAsync(string email);
        public Task<User> LoadRolesAsync(User user);
        public Task<User> VerifyEmail(User user);
        public void ChangeUser(User user, string name = null, string email = null, string password = null);
        public Task<User> CreateUserAsync(string name, string email, string password);
    }
}
