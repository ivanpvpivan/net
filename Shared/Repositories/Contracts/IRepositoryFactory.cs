﻿using System.Threading.Tasks;

namespace Shared.Repositories.Contracts
{
    public interface IRepositoryFactory
    {
        IUserRepository User { get; }

        ISessionRepository Session { get; }

        IUserVerificationRepository UserVerification { get; }

        IPasswordResetRepository PasswordReset { get; }
        Task SaveAsync();
    }
}
