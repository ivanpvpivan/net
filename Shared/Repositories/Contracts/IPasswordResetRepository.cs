﻿using System.Threading.Tasks;

using Shared.Models.Entities;

namespace Shared.Repositories.Contracts
{
    public interface IPasswordResetRepository : IRepository<PasswordReset>
    { 
        public PasswordReset CreateToken(string email);
        public Task<PasswordReset> FindByTokenAsync(string token);
    }
}
