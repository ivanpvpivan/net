﻿using Shared.Repositories.Contracts;
using Shared.Helpers;
using Shared.Models;
using Shared.Models.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Shared.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(AppDbContext context): base(context)
        {

        }

        public Task<User> GetUserByIdAsync(long id) =>
            GetByCondition(u => u.Id == id).FirstOrDefaultAsync();

        public Task<User> GetUserWithRolesByIdAsync(long id) =>
           GetByCondition(u => u.Id == id).Include(u => u.Roles).ThenInclude(ur => ur.Role).FirstOrDefaultAsync();

        public Task<User> GetUserByEmailAsync(string email) =>
            GetByCondition(user => user.Email == email).FirstOrDefaultAsync();

        public Task<User> GetUserWithRolesByEmailAsync(string email) =>
            GetByCondition(user => user.Email == email).Include(u=>u.Roles).ThenInclude(r=>r.Role).FirstOrDefaultAsync();

        public Task<User> GetTrackedUserByEmailAsync(string email) =>
            GetByConditionWithTracking(user => user.Email == email).FirstOrDefaultAsync();

        public Task<User> GetTrackedUserWithRolesByEmailAsync(string email) =>
            GetByConditionWithTracking(user => user.Email == email).Include(u => u.Roles).ThenInclude(r => r.Role).FirstOrDefaultAsync();

        public async Task<User> CreateUserAsync(string name, string email, string password)
        {
            User user = new User
            {
                Name = name,
                Password = PasswordHelper.Make(password),
                Email = email,
                //Roles = new List<UserRole>()
            };

            Insert(user);

            Role role = await _context.Roles.Where(q => q.SystemName == "unverified").FirstOrDefaultAsync();

            UserRole ur = new UserRole
            {
                User = user,
                Role = role
            };

            user.Roles.Add(ur);

            return user;
        }

        public async Task<User> LoadRolesAsync(User user)
        {
            await _context.Entry(user)
                .Collection(u => u.Roles)
                .LoadAsync();

            return user;
        }

        public async Task<User> VerifyEmail(User user)
        {
            user.VerifiedAt = DateTime.UtcNow;

            var roles = await _context.Roles.Where(q => q.SystemName == "unverified" || q.SystemName == "user").ToListAsync();

            Role unver = roles.Where(r => r.SystemName.Trim() == "unverified").First();
            Role us = roles.Where(r => r.SystemName.Trim() == "user").First();

            var ur = user.Roles.Where(ur => ur.RoleId == unver.Id).First();
            user.Roles.Remove(ur);

            user.Roles.Add(new UserRole {
                User = user,
                Role = us
            });

            Update(user);

            return user;
        }

        public void ChangeUser(User user, string name = null, string email = null, string password = null)
        {
            if (name != null)
                user.Name = name;

            if (email != null)
                user.Email = email;

            if (password != null)
                user.Password = PasswordHelper.Make(password);

            Update(user);
        }

        public override void Update(User entity)
        {
            entity.UpdatedAt = DateTime.UtcNow;

            base.Update(entity);
        }
    }
}
