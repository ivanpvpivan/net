﻿using System.Threading.Tasks;
using System.Text.RegularExpressions;

using Microsoft.EntityFrameworkCore;

using Shared.Models;
using Shared.Helpers;
using Shared.Models.Entities;
using Shared.Repositories.Contracts;

namespace Shared.Repositories
{
    public class UserVerificationRepository : BaseRepository<UserVerification>, IUserVerificationRepository
    {
        public UserVerificationRepository(AppDbContext context): base(context)
        {
        }

        public UserVerification CreateToken(string email)
        {
            UserVerification token = new UserVerification
            {
                Email = email,
                Token = Regex.Replace(Salt.Create().Substring(0, 20), @"[\\/\+]", "a")
            };

            Insert(token);
            return token;
        }

        public Task<UserVerification> FindByTokenAsync(string token) =>
            GetByCondition(uv => uv.Token == token).FirstOrDefaultAsync();
       
    }
}
