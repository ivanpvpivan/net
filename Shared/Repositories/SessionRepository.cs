﻿using System;
using System.Net;
using Shared.Repositories.Contracts;
using Shared.Models;
using Shared.Helpers;
using Shared.Models.Entities;

namespace Shared.Repositories
{
    public class SessionRepository : BaseRepository<Session>, ISessionRepository
    {
        public SessionRepository(AppDbContext context): base(context)
        {

        }

        public Session GenerateSession(string userAgent, IPAddress ip, uint ttl)
        {
            Session session = new Session
            {
                IPAddress = ip,
                UserAgent = userAgent,
                ExpiredAfter = DateTime.UtcNow.AddMinutes(ttl),
                RefreshToken = Salt.Create().Substring(0, 20),
            };

            return session;
        }

        public Session RefreshSession(Session session, uint ttl)
        {
            session.ExpiredAfter = DateTime.UtcNow.AddMinutes(ttl);
            session.RefreshToken = Salt.Create().Substring(0, 20);

            Update(session);

            return session;
        }

        public void DisableSession(Session session)
        {
            session.IsActive = false;

            Update(session);
        }

        public override void Update(Session session)
        {
            session.UpdatedAt = DateTime.UtcNow;

            base.Update(session);
        }
    }
}
