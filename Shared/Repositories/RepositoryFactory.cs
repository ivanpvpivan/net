﻿using Shared.Repositories.Contracts;
using Shared.Models;
using System.Threading.Tasks;

namespace Shared.Repositories
{
    public class RepositoryFactory : IRepositoryFactory
    {
        private AppDbContext _context;
        private IUserRepository userRepository;
        private ISessionRepository sessionRepository;
        private IUserVerificationRepository userVerificationRepository;
        private IPasswordResetRepository passwordResetRepository;

        public RepositoryFactory(AppDbContext context)
        {
            _context = context;
        }

        public IUserRepository User
        {
            get
            {
                if (userRepository == null)
                    userRepository = new UserRepository(_context);

                return userRepository;
            }
        }

        public ISessionRepository Session
        {
            get
            {
                if (sessionRepository == null)
                    sessionRepository = new SessionRepository(_context);

                return sessionRepository;
            }
        }

        public IUserVerificationRepository UserVerification
        {
            get
            {
                if (userVerificationRepository == null)
                    userVerificationRepository = new UserVerificationRepository(_context);

                return userVerificationRepository;
            }
        }

        public IPasswordResetRepository PasswordReset
        {
            get
            {
                if (passwordResetRepository == null)
                    passwordResetRepository = new PasswordResetRepository(_context);

                return passwordResetRepository;
            }
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }
    }
}
