﻿using Microsoft.EntityFrameworkCore;
using Shared.Repositories.Contracts;
using Shared.Models;
using System.Linq;
using System.Linq.Expressions;
using System;

namespace Shared.Repositories
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly AppDbContext _context;
        private DbSet<TEntity> entities;

        public BaseRepository(AppDbContext context)
        {
            _context = context;
            entities = context.Set<TEntity>();
        }

        public IQueryable<TEntity> GetAll()
        {
            return entities.AsNoTracking();
        }

        public IQueryable<TEntity> GetByCondition(Expression<Func<TEntity, bool>> expression)
        {
            return entities.Where(expression).AsNoTracking();
        }

        public IQueryable<TEntity> GetByConditionWithTracking(Expression<Func<TEntity, bool>> expression)
        {
            return entities.Where(expression);
        }

        public virtual void Insert(TEntity entity)
        {
            entities.Add(entity);
        }

        public virtual void Update(TEntity entity)
        {
            entities.Update(entity);
        }
        public virtual void Delete(TEntity entity)
        {
            entities.Remove(entity);
        }
    }
}
