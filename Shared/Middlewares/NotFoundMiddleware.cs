﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

using Shared.Helpers.Response;

namespace Shared.Middlewares
{
    public class NotFoundMiddleware
    {
        private readonly RequestDelegate _next;

        public NotFoundMiddleware(RequestDelegate next)
        {
            _next = next;
        }
        public async Task InvokeAsync(HttpContext context)
        {
            await _next(context);

            if (context.Response.StatusCode == 404)
            {

                ApiResponse resp = new ApiResponse(msg: "not_found", code: 404);
                await resp.ExecuteResultAsync(new ActionContext
                {
                    HttpContext = context
                });

                await context.Response.CompleteAsync();
            }
        }
    }
}
