﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;

using Shared.Auth.Contracts;
using Shared.Helpers.Response;

namespace Shared.Middlewares
{
    public class CheckTokenMiddleware
    {
        private readonly ITokenManager _tokenManager;
        private readonly RequestDelegate _next;

        public CheckTokenMiddleware(RequestDelegate next, ITokenManager tokenManager)
        {
            _tokenManager = tokenManager;
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            if (await _tokenManager.IsCurrentActiveToken())
            {
                await _next(context);
                return;
            }

            ApiResponse resp = new ApiResponse(msg: "token_revoked", code: 401);
            await resp.ExecuteResultAsync(new ActionContext
            {
                HttpContext = context
            });

            await context.Response.CompleteAsync();

        }
        
    }
}
