﻿using System;
using System.Linq;
using System.Text;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.IdentityModel.Tokens.Jwt;

using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Primitives;
using Microsoft.Extensions.Caching.Distributed;

using Shared.Helpers.Config;
using Shared.Auth.Contracts;

namespace Shared.Auth
{
    public class TokenManager : ITokenManager
    {
        private readonly IDistributedCache cache;
        private readonly IHttpContextAccessor httpContextAccessor;
        private readonly KeySettingsSection settings;

        public TokenManager(IDistributedCache cache,
            IHttpContextAccessor httpContextAccessor,
            IOptions<KeySettingsSection> option)
        {
            this.cache = cache;
            this.httpContextAccessor = httpContextAccessor;
            settings = option.Value;
        }

        public async Task<bool> IsCurrentActiveToken()
        => await IsActiveAsync(GetCurrentToken());

        public async Task DeactivateCurrentAsync()
            => await DeactivateAsync(GetCurrentToken());

        public async Task<bool> IsActiveAsync(string token)
            => await cache.GetStringAsync(GetKey(token)) == null;

        public async Task DeactivateAsync(string token)
            => await cache.SetStringAsync(GetKey(token),
                " ", new DistributedCacheEntryOptions
                {
                    AbsoluteExpirationRelativeToNow =
                        TimeSpan.FromMinutes(settings.Ttl)
                });

        public string GenerateToken(IEnumerable<Claim> claims, string issuer)
        {
            SigningCredentials creds = new SigningCredentials(
                new SymmetricSecurityKey(Encoding.UTF8.GetBytes(settings.AppKey)),
                SecurityAlgorithms.HmacSha512Signature);

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();

            SecurityToken token = tokenHandler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = issuer,
                Audience = "user",
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddMinutes(settings.Ttl),
                SigningCredentials = creds
            });

            return tokenHandler.WriteToken(token);
        }

        public void ValidateToken(out ClaimsPrincipal principal)
        {
            var validationParams = ITokenManager.GetTokenValidationParameters(settings.Issuers, settings.AppKey);
            validationParams.ValidateLifetime = false;

            var tokenHandler = new JwtSecurityTokenHandler();
            string accessToken = GetCurrentToken();

            SecurityToken securityToken;

            principal = tokenHandler.ValidateToken(accessToken, validationParams, out securityToken);

            var jwtSecurityToken = securityToken as JwtSecurityToken;

            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha512, StringComparison.InvariantCultureIgnoreCase))
                throw new SecurityTokenException("Invalid token!");
        }

        private string GetCurrentToken()
        {
            var authorizationHeader = httpContextAccessor
                .HttpContext.Request.Headers["authorization"];

            return authorizationHeader == StringValues.Empty
                ? string.Empty
                : authorizationHeader.Single().Split(" ").Last();
        }

        private static string GetKey(string token)
        {
            string key;
            using (SHA256 sha256Hash = SHA256.Create())
            {
                byte[] bytes = sha256Hash.ComputeHash(Encoding.UTF8.GetBytes(token));
                StringBuilder builder = new StringBuilder();  
                for (int i = 0; i < bytes.Length; i++)  
                {  
                    builder.Append(bytes[i].ToString("x2"));  
                }  
                key = builder.ToString();
            }
            return $"token:{key}:revoked";
        }
    }
}
