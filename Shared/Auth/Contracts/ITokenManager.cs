﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Security.Claims;
using System.Collections.Generic;

using Microsoft.IdentityModel.Tokens;

namespace Shared.Auth.Contracts
{
    public interface ITokenManager
    {
        public static TokenValidationParameters GetTokenValidationParameters(IEnumerable<string> issuers, string appKey)
        {
            return new TokenValidationParameters
            {
                ClockSkew = TimeSpan.Zero,
                ValidateLifetime = true,
                ValidateAudience = true,
                ValidateIssuer = true,
                ValidAudience = "user",
                ValidIssuers = issuers,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(appKey))
            };
        }
        public string GenerateToken(IEnumerable<Claim> claims, string issuer);
        public void ValidateToken(out ClaimsPrincipal principal);
        Task<bool> IsCurrentActiveToken();
        Task DeactivateCurrentAsync();
        Task<bool> IsActiveAsync(string token);
        Task DeactivateAsync(string token);
    }
}
