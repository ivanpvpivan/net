﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

using Microsoft.Extensions.DependencyInjection;

namespace Shared.Extensions
{
    public static class JsonExtension
    {
        public static IMvcBuilder AddCustomJson(this IMvcBuilder mvc)
        {
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };

            return mvc.AddNewtonsoftJson();
        }
    }
}
