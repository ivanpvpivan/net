﻿using System.IO;
using System.Text;

using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

using Shared.Helpers;
using Shared.Helpers.Config;

namespace Shared.Extensions
{
    public static class KeyConfigExtension
    {
        private static string ConfigDir = "/configs/";
        private static string KeyFile = "key_settings.json";
        public static IHostBuilder ConfigureKeyConfig(this IHostBuilder builder) => 
            builder.ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddJsonFile(Directory.GetCurrentDirectory() + ConfigDir + KeyFile, optional: false);
                });

        public static void AddKeyConfig(this IServiceCollection services, IConfiguration config)
        {
            services.Configure<KeySettingsSection>(options =>
            {
                config.GetSection("Keys").Bind(options);
            });
        }
        public static IApplicationBuilder UseKeyConfigDefaultGenerator(this IApplicationBuilder builder, IConfiguration config)
        {
            string key = config["Keys:AppKey"];
            if (key != null && key != "")
                return builder;

            string keyFilePath = Directory.GetCurrentDirectory() + ConfigDir + KeyFile;

            KeySectionContainer keyContainer = null;
            using (StreamReader file = new StreamReader(keyFilePath, Encoding.Default))
            {
                keyContainer = JsonConvert.DeserializeObject<KeySectionContainer>(file.ReadToEnd());
            }

            if (keyContainer != null && (keyContainer.Keys.AppKey == null || keyContainer.Keys.AppKey == ""))
            {
                keyContainer.Keys.AppKey = Salt.Create();
                config["Keys:AppKey"] = keyContainer.Keys.AppKey;
                using (StreamWriter file = new StreamWriter(keyFilePath, false, Encoding.Default))
                {
                    file.Write(JsonConvert.SerializeObject(keyContainer, new JsonSerializerSettings
                    {
                        Formatting = Formatting.Indented,
                        ContractResolver = new DefaultContractResolver
                        {
                            NamingStrategy = new DefaultNamingStrategy()
                        }
                    }));
                }
            }

            return builder;
        }
    }
}
