﻿using System.Linq;
using System.Collections.Generic;

using Microsoft.Extensions.DependencyInjection;

using Shared.Helpers.Response;

namespace Shared.Extensions
{
    public static class ApiModelValidationResponseExtension
    {
        public static IMvcBuilder AddCustomValidationErrors(this IMvcBuilder mvc) =>
            mvc.ConfigureApiBehaviorOptions(options =>
            {
                options.InvalidModelStateResponseFactory = actionContext =>
                {
                    Dictionary<string, object> props = new Dictionary<string, object>();
                    actionContext.ModelState
                        .Where(modelError => modelError.Value.Errors.Count > 0)
                        .Select(modelError =>
                        {
                            Dictionary<string, object> keyValues = new Dictionary<string, object>();
                            keyValues.Add(modelError.Key, modelError.Value.Errors.Select(item => item.ErrorMessage).ToList());

                            return keyValues;
                        })
                        .ToList()
                        .ForEach(item =>
                        {
                            props = props.Concat(item).ToDictionary(x => x.Key, x => x.Value);
                        });

                    return new ApiResponse(msg: "validation_failed", props: props, code: 400);
                };
            });
    }
}
