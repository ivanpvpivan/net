﻿using System.Collections.Generic;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;

using Shared.Auth;
using Shared.Auth.Contracts;
using Shared.Helpers.Response;


namespace Shared.Extensions
{
    public static class AuthTokenExtension
    {
        public static void AddTokenAuth(this IServiceCollection services, IConfiguration config)
        {
            services.AddAuthentication(options => {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.SaveToken = true;
                options.RequireHttpsMetadata = false;

                var issuersSect = config.GetSection("Keys:Issuers");
                List<string> issuers = new List<string>();
                foreach (var issuer in issuersSect.AsEnumerable())
                    if (issuer.Value != null)
                        issuers.Add(issuer.Value);

                options.TokenValidationParameters = ITokenManager.GetTokenValidationParameters(issuers, config["Keys:AppKey"]);

                options.Events = new JwtBearerEvents()
                {
                    OnChallenge = c =>
                    {
                        c.HandleResponse();
                        var err = c.Error;
                        var failure = c.AuthenticateFailure;
                        if (failure is SecurityTokenException)
                        {
                            ApiResponse resp = new ApiResponse(msg: "token_expired", code: 401);
                            return resp.ExecuteResultAsync(new ActionContext
                            {
                                HttpContext = c.HttpContext
                            });
                        }
                        else
                        {
                            ApiResponse resp = new ApiResponse(msg: "unauthorized", code: 401);
                            return resp.ExecuteResultAsync(new ActionContext
                            {
                                HttpContext = c.HttpContext
                            });
                        }
                    }
                };
            });

            services.AddTransient<ITokenManager, TokenManager>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
        }
    }
}
