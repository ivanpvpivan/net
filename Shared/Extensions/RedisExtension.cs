﻿using System.IO;

using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;

namespace Shared.Extensions
{
    public static class RedisExtension
    {
        private static string ConfigDir = "/configs/";
        private static string KeyFile = "redis_config.json";
        public static IHostBuilder ConfigureRedisConfig(this IHostBuilder builder) =>
            builder.ConfigureAppConfiguration((hostingContext, config) =>
            {
                config.AddJsonFile(Directory.GetCurrentDirectory() + ConfigDir + KeyFile, optional: false);
            });
        public static void AddRedisCacheOnly(this IServiceCollection services, IConfiguration config)
        {
            services.AddStackExchangeRedisCache(options => options.Configuration = config["Redis:ConnectionString"]);
        }

        public static void AddRedisCacheWithMultiplexer(this IServiceCollection services, IConfiguration config)
        {
            string connectionString = config["Redis:ConnectionString"];
            services.AddSingleton<IConnectionMultiplexer>(ConnectionMultiplexer.Connect(connectionString));
            services.AddStackExchangeRedisCache(options => options.Configuration = connectionString);
        }
    }
}
