﻿using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System.IO;
using Shared.Models;
using Shared.Repositories;
using Shared.Repositories.Contracts;

namespace Shared.Extensions
{
    public static class DatabaseExtension
    {
        private static string ConfigDir = "/configs/";
        private static string KeyFile = "db_config.json";
        public static IHostBuilder ConfigureDBConfig(this IHostBuilder builder) =>
            builder.ConfigureAppConfiguration((hostingContext, config) =>
            {
                config.AddJsonFile(Directory.GetCurrentDirectory() + ConfigDir + KeyFile, optional: false);
            });
        public static void AddPostgreSQL(this IServiceCollection services, IConfiguration config)
        {
            services.AddDbContext<AppDbContext>(options =>
                     options.UseNpgsql(config["PostgreSQL:ConnectionString"]));

            services.AddScoped<IRepositoryFactory, RepositoryFactory>();
        }
    }
}
