﻿using System.ComponentModel.DataAnnotations;

namespace AuthService.Models.RequestModels.Auth
{
    public class RefreshModel
    {
        [StringLength(20, MinimumLength = 20)]
        public string RefreshToken { get; set; }
    }
}
