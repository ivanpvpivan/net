﻿using System.ComponentModel.DataAnnotations;

namespace AuthService.Models.RequestModels.Auth
{
    public class RegisterModel
    {
        [Required, StringLength(50, MinimumLength = 3)]
        public string Name { get; set; }

        [Required, StringLength(50)]
        public string Email { get; set; }

        [Required, MinLength(6)]
        public string Password { get; set; }

    }
}
