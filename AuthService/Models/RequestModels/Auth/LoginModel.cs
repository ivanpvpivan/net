﻿using System.ComponentModel.DataAnnotations;

namespace AuthService.Models.RequestModels.Auth
{
    public class LoginModel
    {
        [Required]
        [StringLength(50)]
        [EmailAddress]
        public string Email { get; set; }

        [Required, MinLength(6)]
        public string Password { get; set; }
    }
}
