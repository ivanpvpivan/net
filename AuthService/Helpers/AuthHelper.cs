﻿using System.Security.Claims;
using System.Collections.Generic;

using Microsoft.AspNetCore.Http;

using Shared.Auth.Contracts;
using Shared.Models.Entities;
using Shared.Repositories.Contracts;

namespace AuthService.Helpers
{
    public class AuthHelper
    {
        private readonly IRepositoryFactory repository;

        public AuthHelper(IRepositoryFactory repository)
        {
            this.repository = repository;
        }
        public string Authenticate(ITokenManager tokenManager, User user, Session session, string issuer = "localhost:5001/v1/auth/login")
        {
            List<Claim> claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.Name)
            };

            foreach (var role in user.Roles)
                claims.Add(new Claim(ClaimTypes.Role, role.Role.SystemName.Trim()));

            claims.Add(new Claim("sid", session.Id.ToString()));

            return tokenManager.GenerateToken(claims, issuer);
        }

        public Session CreateSession(uint ttl, HttpRequest request)
        {
            var userAgent = request.Headers["User-Agent"].ToString().Trim();
            userAgent = userAgent.Length > 255 ? userAgent.Substring(0, 255) : userAgent;

            return repository.Session.GenerateSession(userAgent,
                request.HttpContext.Connection.RemoteIpAddress,
                ttl);
        }
    }
}
