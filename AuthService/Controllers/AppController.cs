﻿using Microsoft.AspNetCore.Mvc;
using Shared.Helpers.Response;
using Shared.Repositories.Contracts;

namespace AuthService.Controllers
{
    public class AppController : ControllerBase
    {
        protected readonly IRepositoryFactory repository;

        public AppController(IRepositoryFactory repository)
        {
            this.repository = repository;
        }

        protected IActionResult AnyResponse(object Content = null, object Props = null, string Msg = "success", ushort Code = 200)
        {
            return new ApiResponse(Content, Props, Msg, Code);
        }

        protected IActionResult SuccessResponse(object Content = null, object Props = null)
        {
            return AnyResponse(Content, Props);
        }

        protected IActionResult DeletedResponse()
        {
            return AnyResponse(Code: 204);
        }

        protected IActionResult CreatedResponse(object Content = null)
        {
            return AnyResponse(Content:Content, Msg:"created", Code: 201);
        }

        protected IActionResult NotFoundResponse(object Props = null)
        {
            return AnyResponse(Props: Props, Msg: "not_found", Code: 404);
        }

        protected IActionResult ValidationFailedResponse(object Props = null, string Msg = "validation_error")
        {
            return AnyResponse(Props: Props, Msg: Msg, Code: 400);
        }
    }
}
