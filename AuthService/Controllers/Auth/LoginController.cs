﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;

using Shared.Helpers;
using Shared.Auth.Contracts;
using Shared.Helpers.Config;
using Shared.Models.Entities;
using Shared.Repositories.Contracts;

using AuthService.Helpers;
using AuthService.Models.RequestModels.Auth;

namespace AuthService.Controllers.Auth
{
    [Route("v1/auth/[controller]")]
    [ApiController]
    public class LoginController : AppController
    {
        private readonly AuthHelper helper;
        private readonly KeySettingsSection config;
        private readonly ITokenManager tokenManager;

        public LoginController(IRepositoryFactory repository,
                        IOptions<KeySettingsSection> option,
                        ITokenManager manager): base(repository)
        {
            config = option.Value;
            tokenManager = manager;
            helper = new AuthHelper(repository);
        }

        [HttpPost]
        public async Task<IActionResult> Index([FromForm] LoginModel model)
        {
            User user = await repository.User.GetUserWithRolesByEmailAsync(model.Email);

            if (user == null)
                return ValidationFailedResponse(new { Error = "Email or password is wrong" });

            if (!PasswordHelper.Validate(model.Password, user.Password))
                return ValidationFailedResponse(new { Error = "Email or password is wrong" });

            var session = helper.CreateSession(config.RefreshTtl, Request);
            user.Sessions.Add(session);

            repository.User.Update(user);

            await repository.SaveAsync();

            string token = helper.Authenticate(tokenManager, user, session);

            return SuccessResponse(new
            {
                user.Id,
                user.Name,
                user.Email,

                accessToken = token,
                refreshToken = session.RefreshToken
            });
        }
    }
}
