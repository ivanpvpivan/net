﻿using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;

using Shared.Auth.Contracts;
using Shared.Helpers.Config;
using Shared.Repositories.Contracts;

using AuthService.Helpers;
using AuthService.Models.RequestModels.Auth;

namespace AuthService.Controllers.Auth
{
    [Route("v1/auth/[controller]")]
    [ApiController]
    public class RegisterController : AppController
    {
        private readonly AuthHelper helper;
        private readonly KeySettingsSection config;
        private readonly ITokenManager tokenManager;

        public RegisterController(IRepositoryFactory repository,
                        IOptions<KeySettingsSection> option,
                        ITokenManager manager) : base(repository)
        {
            config = option.Value;
            tokenManager = manager;
            helper = new AuthHelper(repository);
        }

        [HttpPost]
        public async Task<IActionResult> Index([FromForm] RegisterModel model)
        {
            var user = await repository.User.GetByCondition(u => u.Name == model.Name || u.Email == model.Email).FirstOrDefaultAsync();

            if (user != null)
                return ValidationFailedResponse(new { Error = "User with those credentials already exists" });

            user = await repository.User.CreateUserAsync(model.Name, model.Email, model.Password);

            var session = helper.CreateSession(config.RefreshTtl, Request);

            user.Sessions.Add(session);

            var emailToken = repository.UserVerification.CreateToken(model.Email);

            await repository.SaveAsync();

            string token = helper.Authenticate(tokenManager, user, session);

            return SuccessResponse(new
            {
                user.Id,
                user.Name,
                user.Email,

                accessToken = token,
                refreshToken = session.RefreshToken
            });
        }
    }
}
