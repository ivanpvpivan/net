﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;

using Shared.Auth.Contracts;
using Shared.Helpers.Config;
using Shared.Models.Entities;
using Shared.Repositories.Contracts;

using AuthService.Helpers;
using AuthService.Models.RequestModels.Auth;

namespace AuthService.Controllers.Auth
{
    [Route("v1/auth/[controller]")]
    [ApiController]
    public class RefreshController : AppController
    {
        private readonly AuthHelper helper;
        private readonly KeySettingsSection config;
        private readonly ITokenManager tokenManager;

        public RefreshController(IRepositoryFactory repository,
                        IOptions<KeySettingsSection> option,
                        ITokenManager manager) : base(repository)
        {
            config = option.Value;
            tokenManager = manager;
            helper = new AuthHelper(repository);
        }

        [HttpPost]
        public async Task<IActionResult> Index([FromForm] RefreshModel model)
        {
            Guid sId;
            long uId;
            try
            {
                ClaimsPrincipal principal;

                tokenManager.ValidateToken(out principal);

                uId = long.Parse(principal.FindFirst(ClaimTypes.NameIdentifier)?.Value);
                sId = Guid.Parse(principal.FindFirst("sid")?.Value);
            }
            catch (Exception)
            {
                return ValidationFailedResponse(new { Error = "Access token invalid" });
            }

            Session session = await repository.Session
                .GetByCondition(q => q.Id == sId && q.User.Id == uId)
                .FirstOrDefaultAsync();

            if (session == null)
                return NotFoundResponse(new { Error = "No session found" });

            if (session.IsActive == null || session.IsActive == false)
                return ValidationFailedResponse(new { Error = "Session exited" }, "session_closed");

            if (DateTime.Compare(DateTime.UtcNow, session.ExpiredAfter) > 0)
            {
                repository.Session.DisableSession(session);
                await repository.SaveAsync();

                return ValidationFailedResponse(new { Error = "Refresh token expired" }, "refresh_token_expired");
            }

            if (string.Compare(session.RefreshToken, model.RefreshToken) != 0)
                return ValidationFailedResponse(new { Error = "Refresh token mismatch" }, "refresh_token_miss");

            session = repository.Session.RefreshSession(session, config.RefreshTtl);

            await repository.SaveAsync();

            var user = await repository.User.GetByCondition(u => u.Id == uId).Include(ur => ur.Roles).ThenInclude(r => r.Role).FirstOrDefaultAsync();

            if (user == null)
                return AnyResponse(Msg: "server_error", Code: 500);

            await tokenManager.DeactivateCurrentAsync();

            string newToken = helper.Authenticate(tokenManager, user, session, "localhost:5001/v1/auth/refresh");

            return SuccessResponse(new
            {
                accessToken = newToken,
                refreshToken = session.RefreshToken
            });
        }
    }
}
