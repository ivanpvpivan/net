﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;

using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

using Shared.Auth.Contracts;
using Shared.Repositories.Contracts;

namespace AuthService.Controllers.Auth
{
    [Authorize]
    [ApiController]
    [Route("v1/auth/[controller]")]
    public class LogoutController : AppController
    {
        private readonly ITokenManager tokenManager;

        public LogoutController(IRepositoryFactory repository,
                        ITokenManager manager) : base(repository)
        {
            tokenManager = manager;
        }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            Guid sId = Guid.Parse(User.FindFirstValue("sid"));
            long uId = long.Parse(User.FindFirstValue(ClaimTypes.NameIdentifier));

            var session = await repository.Session
                .GetByCondition(q => q.Id == sId && q.User.Id == uId)
                .FirstOrDefaultAsync();

            if (session == null)
                return NotFoundResponse(new { Error = "No session found" });

            await tokenManager.DeactivateCurrentAsync();
            repository.Session.DisableSession(session);

            await repository.SaveAsync();

            return SuccessResponse();
        }
    }
}
